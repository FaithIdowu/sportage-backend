const passport = require("passport"),
  LocalStrategy = require("passport-local").Strategy,
  bcrypt = require("bcrypt-nodejs");

passport.serializeUser(function(user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
  User.findOne({ id }, function(error, user) {
    cb(error, user);
  });
});

passport.use(
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password"
    },
    function(email, password, cb) {
      User.findOne({ email: email }, function(err, user) {
        if (err) {
          return cb(err, { message: "LINE 24" });
        }
        if (!user) {
          return cb(null, false, { message: "Email Address not found" });
        }
        bcrypt.compare(password, user.password, function(err, response) {
          if (!response) {
            return cb(null, false, { message: "Invalid Password" });
          }
          return cb(null, user, { message: "SignIn was Successful" });
        });
      });
    }
  )
);
