/**
 * AuthController
 *
 * @description :: Server-side actions for handling incoming reqs.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const passport = require("passport");

module.exports = {
  // The Signin Controller
  async signin(req, res) {
    try {
      passport.authenticate("local", function(err, user, info) {
        if (err || !user) {
          return res.send({
            success: false,
            message: info.message,
            user
          });
        }

        req.login(user, function(err) {
          if (err) res.send(err);
          return res.send({
            message: info.message,
            user
          });
        });
      })(req, res);
    } catch (err) {
      return res.serverError(err);
    }
  },

  // SignOut Function
  signout: function(req, res) {
    req.logOut();
    res.redirect("/");
  },

  // SignUp Function
  async signup(req, res) {
    try {
      //TODO: Form Validation
      // Form fetching Here
      const requiredDetails = [
        "firstName",
        "lastName",
        "password",
        "email",
        "phoneNumber",
        "address"
      ];

      const details = _.pick(req.body, requiredDetails);
      await User.create(details, function(err, user) {
        if (err) {
          return res.send({
            success: false,
            new: function(request, response) {
              response.view();
            },

            login: function(request, response) {
              response.view();
            },
            message: "Internal server error LINE 60",
            err: err
          });
        }

        //TODO: Send Confirmation email to user

        req.login(user, function(err) {
          res.send(err);
          console.warn("User " + user.id + " has logged");
          res.redirect("/");
        });
        return res.json({ data: user, success: true, message: "User created" });
      });
    } catch (err) {
      return res.serverError(err);
    }
  }
};
