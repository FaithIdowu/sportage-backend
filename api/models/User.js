/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
const bcrypt = require("bcrypt-nodejs");

module.exports = {
  attributes: {
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    firstName: {
      type: "string",
      required: true
    },
    lastName: {
      type: "string",
      required: true
    },
    email: {
      type: "string",
      unique: true,
      isEmail: true,
      required: true
    },
    phoneNumber: {
      type: "number",
      unique: true,
      required: true
    },
    address: {
      type: "string",
      required: true
    },
    password: {
      type: "string",
      minLength: 6,
      required: true
    }
  },

  // To ensure the password is not passed back during search
  customToJSON: function() {
    return _.omit(this, ["password"]);
  },

  // To hash the password before storage
  beforeCreate: function(user, cb) {
    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(user.password, salt, null, function(error, hash) {
        if (error) return cb(error);
        user.password = hash;
        return cb();
      });
    });
  }

  // datastore: "mongodb",
  // schema: true
};
